#include "numbers.cpp"
#include <vector>
#include <cassert>

using namespace std;

//typedef double number;

template <class number>
class Matrix {
private:
    vector<vector<number> > values;

    Matrix & signedAdd(const Matrix & other, const bool negative = false) {
        assert(getX() == other.getX() && getY() == other.getY());
        for (int i = 0; i < getY(); ++i) {
            for (int j = 0; j < getX(); ++j) {
                negative ? get(i, j) -= other.get(i, j) : get(i, j) += other.get(i, j);
            }
        }
        return *this;
    }

public:
    Matrix(int x, int y, number initValue = 0) {
        assert(x > 0 && y > 0);
        values.reserve(y);
        for (int i = 0; i < y; ++i) {
            values.push_back(vector<number>(x, initValue));
        }
    }

    Matrix(const Matrix & other) : values(other.values) {}

    Matrix & operator=(const Matrix & other) {
        values = other.values;
        return *this;
    }

    Matrix operator*(const Matrix & other) const {
        assert(getX() == other.getY());
        Matrix res (other.getX(), getY());
        for (int i = 0; i < getY(); ++i) {
            for (int j = 0; j < other.getX(); ++j) {
                for (int k = 0; k < getX(); ++k) {
                	res.get(i, j) += get(i, k) * other.get(k, j);
            	}
            }
        }
        return res;
    }

    Matrix operator*(const number & scalar) const {
        Matrix tmp (*this);
        tmp *= scalar;
        return tmp;
    }

    Matrix operator+(const Matrix & other) const {
        Matrix tmp (*this);
		tmp += other;
        return tmp;
    }

    Matrix operator-(const Matrix & other) const {
        Matrix tmp (*this);
        tmp -= other;
        return tmp;
    }

    Matrix & operator*=(const number & scalar) {
        for (int i = 0; i < getY(); ++i) {
            for (int j = 0; j < getX(); ++j) {
                get(i, j) *= scalar;
            }
        }
	return *this;
    }

    Matrix & operator*=(const Matrix & other) {
        const Matrix & tmp = *this * other;
        values = tmp.values;
        return *this;
    }

    Matrix & operator+=(const Matrix & other) {
        return signedAdd(other);
    }

    Matrix & operator-=(const Matrix & other) {
        return signedAdd(other, true);
    }

    Matrix transpose() const {
        Matrix res (getY(), getX());
        for (int i = 0; i < getY(); ++i) {
            for (int j = 0; j < getX(); ++j) {
                res.get(j, i) = get(i, j);
            }
        }
        return res;
    }

    Matrix getVector(int i) const {
        assert(i >= 0 && i < getY());
        Matrix res (1, getX());
        for (int j = 0; j < getX(); ++j) {
            res.get(j, 0) = get(i, j);
        }
        return res;
    }

    void fillDiagonal(const number & value) {
        assert(getX() == getY());
        for (int i = 0; i < getX(); ++i) {
            set(i, i, value);
        }
    }

    void set(int row, int col, const number & val) {
        assert(row >= 0 && row < getY() && col >= 0 && col < getX());
        values[row][col] = val;
    }

    number & get(int row, int col) {
        assert(row >= 0 && row < getY() && col >= 0 && col < getX());
        return values[row][col];
    }

    number get(int row, int col) const {
        assert(row >= 0 && row < getY() && col >= 0 && col < getX());
        return values[row][col];
    }

    //todo number implicit coversion
    number & toScalar() {
        assert(getX() == 1 && getY() == 1);
        return get(0, 0);
    }

    number toScalar() const {
        assert(getX() == 1 && getY() == 1);
        return get(0, 0);
    }

    int getX() const {
        assert(!values.empty());
        return values[0].size();
    }

    int getY() const {
        return values.size();
    }

    ll getBitSize() const {
        ll res = 0;
        for (int i = 0; i < getY(); ++i) {
            for (int j = 0; j < getX(); ++j) {
                res += bitSizeOf(get(i, j));
                //res += /*get(i, j).getBitSize();*/64;
            }
        }
        return res;
    }

    int ltEqWithIndex(const Matrix & other) const {
        assert(getX() == 1 && other.getX() == 1 && getY() == other.getY());
        for (int i = 0; i < getY(); ++i) {
            if (get(i, 0) > other.get(i, 0)) return i;
        }
        return -1;
    }

    friend ostream & operator<<(ostream & os, const Matrix m) {
        os << "{";
        for (int i = 0; i < m.getY(); ++i) {
            os << "{" << m.values[i][0];
            for (int j = 1; j < m.getX(); ++j) {
                os << ", " << m.values[i][j];
            }
            os << "}";
            if (i != m.getY()-1) os << ", ";
        }
        os << "}";
	return os;
    }
};


