#include <bits/stdc++.h>
using namespace std;
typedef long long int ll;
typedef long double ld;
typedef unsigned long long int unsll;
//typedef unsigned long long int ull;
typedef unsigned int ull;
const ull MAXULL=numeric_limits<ull>::max();
const ll BITSIZE = sizeof(ull)*8;
ll RND=20;

struct Integer;

void cutZeros(vector<ull> &v){
	while(v.size() > 1 && v[v.size()-1]==0)v.pop_back();
}
template <class T>
T gcd(T AA, T BB) {
	T A=AA,B=BB;
	if(A<0)A=-A;
	if(B<0)B=-B;
	if(B<A)swap(A,B);
#ifdef OPER
	cerr<<"start gcd "<<A<<' '<<B<<endl;
#endif
	const ll itw = 20000;
	ll it=0;//, first = A.getBitSize();
//	cout<<"gcd of "<<A.v.size()<<" and "<<B.v.size()<<" - digit numbers"<<endl;
	while (!A.zero()) {
		T C = A;
		A = B % A;
		B = C;
//		if(++it == 3 && A.getBitSize()==first){
//			return 1;
//		}
		if((++it % itw) == 0){
			if(A<B)swap(A,B);
			cerr<<"WARNING! computation of gcd is already running "<<it<<" iterations"<<endl;
			cerr<<"original parameters: "<<AA<<endl<<"AND: "<<BB<<"\n";
			cerr<<"parameters are\nFIRST: "<<A<<endl;
			cerr<<"AND SECOND: "<<B<<endl;
			cerr<<"CHECK THAT A%B=C\n";
			cerr<<"C: "<<(A%B)<<endl;
			cerr<<"------END OF WARNING MESSAGE------\n"<<endl;
			//it=0;
		}
	}
#ifdef OPER
	cerr<<"end gcd "<<A<<' '<<B<<endl;
#endif
	return B;
}


/*
// trivial integer implementation with long long variable
struct Integer{
	ll v;
	Integer(ll v=0):v(v){}
	Integer(const Integer &i):v(i.v){}
	Integer operator+(const Integer &a)const{return {v+a.v};}
	Integer operator-(const Integer &a)const{return {v-a.v};}
	Integer operator*(const Integer &a)const{return {v*a.v};}
	Integer operator/(const Integer &a)const{return {v/a.v};}
	Integer operator%(const Integer &a)const{return {v%a.v};}
	Integer &operator=(const Integer &a){v=a.v;return *this;}
	Integer &operator+=(const Integer &a){v+=a.v;return *this;}
	Integer &operator-=(const Integer &a){v-=a.v;return *this;}
	Integer &operator*=(const Integer &a){v*=a.v;return *this;}
	Integer &operator/=(const Integer &a){v/=a.v;return *this;}
	bool    operator!=(const Integer &a){return v!=a.v;}
	friend ostream &operator<<(ostream &os,const Integer &i){
		os<<i.v;
		return os;
	}
};*/
struct Integer{
	vector<ull> v;
	int sign; // +1 / -1
	const unsll base = ((unsll)MAXULL)+1;
	Integer(ll val){
		sign=(val>=0?+1:-1);
		if(val<0)val=-val;
		if(val==0)v.push_back(0);
		while(val>0){
			ll res=val % base;
			v.push_back(res);
			val /= base;
		}
		consistent();
	}
	Integer(int val=0):Integer((ll)val){}
	Integer(vector<ull> v,int sign=1):v(v),sign(sign){assert(sign==1||sign==-1);}
	Integer(const Integer &i):v(i.v),sign(i.sign){assert(sign==1||sign==-1);}
	void consistent()const{
		assert(v.size()!=0);
		bool f=v[v.size()-1]!=0 || v.size()==1;
		if(!f)cerr<<*this<<endl;
		assert(v[v.size()-1]!=0 || v.size()==1);
		assert(sign==1||sign==-1);
		assert(v.size()>=1);
	}
	inline bool checkOverflow(const ull &a,const ull &b, bool over)const{
		return (MAXULL==a)?(b||over):(MAXULL-(a+over) < b);
	}
	Integer absval()const{return {v,1};}
	Integer plus(const Integer &a)const{
		if(zero())return Integer(a);
		bool over=false;
		ll i=0, j=0, k=0;
		ll I=v.size(), J=a.v.size();
		vector<ull> res(1+max(I,J));
		for(; i<I && j<J; ++i,++j){
			res[k++]=v[i]+a.v[j]+over;
			over = checkOverflow(v[i],a.v[j],over);
		}
		for(;i<I;++i){
			res[k++]=v[i]+over;
			over = checkOverflow(v[i],0,over);
		}
		for(;j<J;++j){
			res[k++]=a.v[j]+over;
			over = checkOverflow(0,a.v[j],over);
		}
		if(over) res[k++]=1;
		cutZeros(res);
		Integer rr={res,sign};
#ifdef OPER
		cerr<<(*this)<<" + "<<a<<" = "<<rr<<endl;
#endif
		rr.consistent();
		return rr;
	}
	inline bool checkUnderflow(const ull &a,const ull &b, bool under)const{
		return (a < b) || (a - b < under);
	}
	Integer minus(const Integer &c)const{
		bool less=abscmp(c) == -1; // less in abs value?
		const Integer &b=less?*this:c; // b is smaller
		const Integer &a=less?c:*this; // a is bigger
		bool under=false;
		ll i=0, j=0, k=0;
		ll I=a.v.size(), J=b.v.size();
		vector<ull> res(1+max(I,J));
		for(; i<I && j<J; ++i,++j){
			res[k++]=a.v[i]-(b.v[j] + under);
			under = checkUnderflow(a.v[i], b.v[j], under);
		}
		for(;i<I;++i){
			res[k++]=a.v[i]-under;
			under = checkUnderflow(a.v[i], 0, under);
		}
		if(j<J)assert(false);
		cutZeros(res);
		Integer rr={res,a.sign};
#ifdef OPER
		cerr<<a<<" - "<<b<<" = "<<rr<<endl;
#endif
		assert(rr.abscmp(0)>=0);
		return rr;
	}
	Integer absTimes(const Integer &a)const{
		ll I=v.size(),J=a.v.size();
		// beware of overflow / use unsinged long long
		vector<unsll> res(I+J);
		for(ll i=0;i<I;++i){
			if(v[i] == 0)continue;
			for(ll j=0;j<J;++j){
				if(a.v[j] == 0)continue;
//				cout<<"to "<<(i+j)<<" "<<v[i]<<" * "<<a.v[j]<<endl;
				unsll val=v[i]*(ll)a.v[j];
				ll low=val & (base-1);
				ll high=val >> BITSIZE;
				ll q=i+j;
				res[q]+=low;
				res[q+1]+=high;
				while(res[q]>=base || q == i+j){
					ll rest=res[q]%base;
					res[q+1]+=res[q]/base;
					res[q]=rest;
					q++;
				}
			}
		}
		while(res.size() > 1 && res[res.size()-1]==0)res.pop_back();
		vector<ull> r(res.size());
		for(ll i=0;i<(ll)res.size();++i){
			r[i]=res[i];
		}
		cutZeros(r);
		return {r,1};
	}
	// times for division:
	// original  20m (still not finished)
	// new       1m 23s
	Integer absDiv(const Integer &c)const{
		if(c.zero())assert("Divide by zero" && false);
		if(zero())return Integer(0);
		if(c.abscmp(*this)==1)return Integer(0);
		const Integer cc=c.absval();
//		cout<<(*this)<<' '<<c<<endl;

//		cout<<"cc: ";cc.printBits();
//		cout<<"ths:";(*this).printBits();
		Integer res(0), tmp(0);
		for(ll i=v.size()-1; i>=0; --i){
			ll mask=(ull)1<<(BITSIZE-1);
//			cout<<"MASK:"<<mask<<endl;
			for(ll j=0; j<BITSIZE; ++j){
				bool isSet = v[i]&mask;
				tmp=(tmp<<1) + (ll)isSet;
				mask>>=1;
				res=res<<1;
				// test if c can divide tmp (is not smaller)
//				if(isSet){
//					cout<<"it:"<<i<<' '<<j<<endl;
//					cout<<"tmp:";tmp.printBits();
//				}
				if(tmp.abscmp(cc) != -1){
//					cout<<tmp<<" >= "<<cc<<endl;
					tmp -= cc;
					res += 1;
				}
//				cout<<"res:";res.printBits();
//				cout<<"msk:"<<mask<<endl;
			}
		}
//		cout<<"end\n";
//		cout<<"tmp:";tmp.printBits();
//		cout<<"res:";res.printBits();
		return res;
	}
	void printBits()const{
		for(ll i=v.size()-1; i>=0; --i){
			bitset<32> a(v[i]);
			cout<<a<<' ';
		}
		cout<<endl;
	}
	Integer operator+(const Integer &a)const{ return (sign==a.sign)?plus(a):minus(a); }
	Integer operator-(const Integer &a)const{ return (*this)+(-a); }
	Integer operator-()const{ return {v,-sign}; }
	Integer operator*(const Integer &a)const{
		Integer res=absTimes(a);
		res.sign = sign * a.sign;
		// cout << sign << " " << a.sign << " " << res.sign << endl;
		res.consistent();
		return res;
	}
	Integer operator<<(ll a)const{
		if(a!=1){
			cout<<"operator << "<<a<<" not implemented"<<endl;
			assert(false);
		}
		bool over=false;
		ll sz=v.size();
		vector<ull> res(sz+1);
		for(ll i=0;i<sz;++i){
			res[i] = (((ll)v[i])<<1) | (over?1:0);
			over = v[i]&(base>>1);
		}
		if(over)res[sz]=1;
		cutZeros(res);
		Integer rr={res,sign};
		rr.consistent();
		return rr;
	}
	Integer operator>>(ll a)const{
		if(a==0)return Integer(*this);
		if(a==1){
			bool under=false;
			vector<ull> res(v.size());
			for(ll i=v.size()-1;i>=0;--i){
				res[i] = (v[i]>>1) | (under?base>>1:0);
				under = v[i]&1;
			}
			cutZeros(res);
			Integer rr={res,sign};
			rr.consistent();
			return rr;
		}
		if(a%BITSIZE==0){
			ll sz=max(0ll,(ll)v.size()-a/BITSIZE);
			ll start=v.size()-sz;
			vector<ull> res(sz);
			for(ll i=0; i<sz; ++i){
				res[i]=v[start+i];
			}
			if(res.size()==0)res.push_back(0);
			cutZeros(res);
			Integer rr={res,sign};
			rr.consistent();
			return rr;
		}
		assert("Not implemented"||false);
		return Integer(0);
	}
	Integer operator/(const Integer &a)const{
		Integer res=absDiv(a);
		res.sign=sign*a.sign;
		return res;
	}
	Integer operator%(const Integer &a)const{
		if(a==2) return Integer(vector<ull>(1, v[0]&1),sign);
		Integer q = (*this).absval();
		Integer div = q/a;
		Integer mul = a*div;
		Integer res = minus(mul);
#ifdef OPER
		cerr<<"MODULO: "<<*this<<" % "<<a<<" = "<<res<<" {"<<div<<' '<<mul<<'}'<<endl;
#endif
		if(a.abscmp(1)==0)assert(res.abscmp(0)==0);
		res.sign = sign;
		res.consistent();
		return res;
	}
	Integer &operator=(const Integer &a){
		if(&a == this)return *this;
#ifdef OPER
		cerr<<(*this)<<" = "<<a<<" -> ";
#endif
		v=a.v; sign=a.sign;
#ifdef OPER
		cout<<(*this)<<endl;
#endif
		return *this;
	}
	// quite effective since vector would have been replaced anyway
	Integer &operator+=(const Integer &a){ *this=*this + a; return *this; }
	Integer &operator-=(const Integer &a){ *this=*this - a; return *this; }
	Integer &operator*=(const Integer &a){ *this=*this * a; return *this; }
	Integer &operator/=(const Integer &a){ *this=*this / a; return *this; }
explicit	operator double()const{
		return (double)((ld)*this);
	}
explicit	operator ld()const{
		ld res=0,q=1;
		Integer r=*this;
		while(!r.zero()){
			if(r%2 != 0){
				res+=q;
//				cout<<res<<endl;
			}
			r=r>>1;
			q*=2;
		}
		if(r.sign == -1) res*=-1;
		return res;
	}
	// comparisons
	int     abscmp(const Integer &a)const{
//		cout<<v.size()<<' '<<a.v.size()<<endl;
		if(v.size()!=a.v.size())return v.size()<a.v.size()?-1:1;
		for(ll i=v.size()-1;i>=0;--i){
//			if(v[i]!=a.v[i])cout<<"DIFFER IN: "<<i<<endl;
			if(v[i]!=a.v[i])return v[i]<a.v[i]?-1:1;
		}
		return 0;
	}
	bool    zero()const{
		return v.size()==1 && v[0]==0;
	}
	bool    operator<(const Integer &a)const{
		if(sign!=a.sign) return !(zero() && a.zero()) && sign < a.sign;
		int r=abscmp(a)*sign;
		return r == -1;
	}
	bool    operator==(const Integer &a)const{
		return abscmp(a)==0 && (sign==a.sign || zero());
	}
	bool    operator!=(const Integer &a)const{
		return !(*this == a);
	}
	ll	getBitSize()const {
		return v.size()*BITSIZE+1;
	}
/*friend  string  operator+(string str, const Integer &a){
		Integer rest,tmp=a,dec=10;
		string res="";
		while(!a.zero()){
			rest=tmp%dec;
			break;
			res+=(char)('0'+rest.v[0]);
			tmp/=dec;
		}
		reverse(res.begin(),res.end());
		return str+res;
	}
*/	// output
friend  ostream &operator<<(ostream &os,const Integer &a){
		os<<'('<<(a.sign==1?"":"--");
		for(ll i=0;i<(ll)a.v.size();++i){
			if(i)os<<' ';
			os<<(ll)a.v[a.v.size()-1-i];
		}
		os<<')';
		return os;
	}
friend  ostream &operator<(ostream &os,const Integer &a){
		Integer rest,tmp=a,dec=10;
//		os<a;os<<" = ";
		os<<'('<<(a.sign==1?"":"--");
		if(tmp.abscmp(0)==-1){
			os<<'-';
			tmp=-tmp;
		}
		string res="";
		while(!a.zero()){
			rest=tmp%dec;
			break;
			res+=(char)('0'+rest.v[0]);
			tmp/=dec;
		}
		reverse(res.begin(),res.end());
		os<<res;
		return os;
	}
};
Integer pow(Integer a, Integer b){
	if(a.abscmp(1)==0)return (b%2!=0)?-a:a;
	Integer r=a,res=1;
	while(b.abscmp(0) == 1){
		if(b%2 != 0){
			res*=r;
		}
		r*=r;
		b=b>>1;
	}
	return res;
}
////// Integer [end]


struct Rational{
	Integer c,j;
	Rational(Integer c=0ll,Integer j=1ll):c(c),j(j){}
	Rational(int val):c(val),j(1){}
	Rational(ll val):c(val),j(1){}
//	Rational(ld val){
//		assert("conversion from double not implemented yet" && 0);
//	}
	Rational(const Rational &r):c(r.c),j(r.j){}
	Rational operator+(const Rational &a)const{Rational r={c*a.j+a.c*j, j*a.j};return r.cut();}
	Rational operator-(const Rational &a)const{Rational r={c*a.j-a.c*j, j*a.j};return r.cut();}
	Rational operator*(const Rational &a)const{Rational r={c*a.c, j*a.j};return r.cut();}
	Rational operator/(const Rational &a)const{Rational r={c*a.j, j*a.c};return r.cut();}
	Rational operator-()const{return {-c, j};}
	Rational &operator=(const Rational &a){c=a.c;j=a.j;return *this;}
	Rational &operator+=(const Rational &a){auto q=(*this)+a;(*this)=q;return (*this).cut();}
	Rational &operator-=(const Rational &a){auto q=(*this)-a;(*this)=q;return (*this).cut();}
	Rational &operator*=(const Rational &a){c*=a.c;j*=a.j;return (*this).cut();}
	Rational &operator/=(const Rational &a){c*=a.j;j*=a.c;return (*this).cut();}
	bool     operator<(const Rational &a)const{return c*a.j < j*a.c;}
	bool     operator>(const Rational &a)const{return j*a.c < c*a.j;}
	bool     operator==(const Rational &a)const{return c*a.j == j*a.c;}
	Rational &round(){
		// simple rounding so that the number size doesn't grow indefinitely
		ll n=c.v.size();
		ll m=j.v.size();
		ll mx=max(m,n);
		if(mx>=RND){
			ll shift=BITSIZE*(mx-RND);
			c=c>>shift;
			j=j>>shift;
			if(j.zero())j=1;
		}
		return *this;
	}
	Rational &cut(){
		if(j==Integer(1) || c==Integer(1))return *this;
		Integer g = gcd<Integer>(c,j);
		if(g==Integer(1))return *this;
//		assert((c/g)*g == c);
//		assert((j/g)*g == j);
		c/=g; j/=g;
		return *this;
	}
	ll	getBitSize()const{
		return c.getBitSize() + j.getBitSize();
	}
explicit 	operator double()const{
		return (double)((ld)*this);
	}
explicit 	operator ld()const{
//		cout<<"conversion to long double "<<(ld)c<<' '<<(ld)j<<endl;
//		cout<<"from:"<<c<<endl<<j<<endl;
		return (ld)c/(ld)j;
	}
friend	Rational operator*(ll a,const Rational &r){
		return (r*a).cut();
	}
friend	Rational operator/(ll a,const Rational &r){
		return ((Rational)a/r).cut();
	}
friend  ostream &operator<(ostream &os,const Rational &r){
		os<<'[';
		os<r.c;
		os<<'/';
		os<r.j;
		os<<']';
		return os;
	}
friend  ostream &operator<<(ostream &os,const Rational &r){
		os<<'['<<r.c<<'/'<<r.j<<']';
		return os;
	}
};
Rational sqrt(Rational rat){
	ll nums = max(2ll,(ll)rat.c.v.size());
	//cerr << "nums " << nums << endl;
	vector<ull> w(nums/2,0);
	//cerr << "w " << w.size() << endl;
	//estimate of result
	w[w.size()-1]=1;
	Rational res(Integer(w,1));
	Rational dist(1);
	Rational e(1,100);
//	e=e*e; e=e*e; e=e*e;
	Rational ne=-e;
	Rational two(2,1);
//	cout<<"dist: "<<dist<<endl;
	ll it=0;
	const ll mx=1000;
	cerr<<">>> begin sqrt of "<<(ld)rat<<endl;
	while(!(ne < dist && dist < e)){
//		cout<<"cmp: "<<res<<"  =  "<<res*res<<' '<<rat<<endl;
//		cout<<"beg"<<endl;
//		cout<<"avg of "<<res<<' '<<rat/res<<" = "<<(res+rat/res)/two<<endl;
		res = (res+(rat/res))/two;// iterative met. for sqrt = (x + a/x) / 2
		res.cut();
		cerr<<">>: "<<(ld)res<<endl;
		if(++it > mx)break;
		{
			RND=300;
			res.round();
		}
		if(it%4==0){
			dist = rat-res*res;
			cerr<<">> dist: "<<(ld)dist<<endl;
		}
//		cout<<"DONE SQRT ITERATION "<<it<<endl;
//		cout<<"----------------"<<endl;
//		cout<<"e"<<endl;
	}
	cerr<<">>> end sqrt: "<<(ld)res<<endl;
//	if(it<=mx){
//		cout<<"ok comparing "<<ne<<' '<<dist<<' '<<e<<endl;
//	}
//	cout<<"finished sqrt with error of: "<<rat<<"\n - "<<res<<"\n : "<<dist<<endl;
	res.cut();
	return res;
}
Rational pow(Rational a, Integer b){
	bool swp=b<0;
	if(swp) b=-b;
	Rational res={pow(a.c,b),pow(a.j,b)};
	if(swp) swap(res.c,res.j);
	return res;
}

ll bitSizeOf(Rational n) {return n.getBitSize();}
ll bitSizeOf(Integer n) {return n.getBitSize();}
ll bitSizeOf(double n) {return sizeof n;}
	

#ifdef MAIN

// for testing
#define FOR(a,b,c) for(int a=b;a<c;++a)
#define F(a) FOR(i,0,a)
#define FF(a) FOR(j,0,a)

int main(){
	/*
	// fibonacci
	Integer v = 1,w = 1,q;
	for(int i = 0; i<100;++i){
		q=v;
		v=v+w;
		w=q;
		cout<<v<<" - "<<w<<" = "<<v-w<<endl;
		cout<<w<<" - "<<v<<" = "<<w-v<<endl;
		cout<<-v<<" - "<<-w<<" = "<<(-v)-(-w)<<endl;
		cout<<-w<<" - "<<-v<<" = "<<(-w)-(-v)<<endl;
	}
	*/
	{
		// check basic rounding
		ll TRND=RND;
		vector<ull> w={2576854995,858993457,1};
		vector<ull> v={2415801558,0,1449049932,3435973828};
		reverse(w.begin(),w.end());
		reverse(v.begin(),v.end());
		Rational r(Integer(w,1),Integer(v,1));
		RND=3;
		r.round();
		vector<ull> ww={2576854995,858993457};
		vector<ull> vv={2415801558,0,1449049932};
		reverse(ww.begin(),ww.end());
		reverse(vv.begin(),vv.end());
		assert(r.c.v.size()==ww.size());
		F(r.c.v.size())assert(r.c.v[i]==ww[i]);
		assert(r.j.v.size()==vv.size());
		F(r.j.v.size())assert(r.j.v[i]==vv[i]);
		RND=TRND;
	}
	// check multiplication and division for every situation
	{
		vector<ull> w={2576854995,858993457};
		vector<ull> v={4026531840,4};
		vector<ull> r={2415801558,0,1449049932,3435973828};
		reverse(w.begin(),w.end());
		reverse(v.begin(),v.end());
		reverse(r.begin(),r.end());
		Integer a(w),b(v),e(r);
//		cout<<"A:  ";a.printBits();
//		cout<<"B:  ";b.printBits();
		Integer c=a*b;
//		cout<<"A*B:";c.printBits();
//		Integer c=gcd(a,b);
//		cout<<c<<endl;
		assert(a*b == e);
		assert(e/a == b);
		assert(e/b == a);
	}
	{
		vector<ull> w={2415801558,0,3119412436,352274365};
		vector<ull> v={4026531840,4};
		reverse(w.begin(),w.end());
		reverse(v.begin(),v.end());
		Integer a(w),b(v);
		cout<<"A:  ";a.printBits();
		cout<<"B:  ";b.printBits();
		Integer c=a%b;
		cout<<"A%B:";c.printBits();
//		Integer c=gcd(a,b);
	}
	// cut
	{
		vector<ull> v={2},w={0,1};
		Rational r({v,1},{w,1});
		r.cut();
	}
	// bitshfit tests
	{
		Integer a(1ll<<(BITSIZE-1));
		a=a<<1;
		assert(a.v[0]==0); assert(a.v[1]==1);
	}
	{
		Integer Q=4,W=6,E=24,qs,ws,q,w,e;
		F(3) FF(3){
			qs=i-1;
			ws=j-1;
			q=Q*qs;
			w=W*ws;
			e=E*ws*qs;
			assert(q*w == e);
			if(!w.zero()) {
				cout<<q<<" = "<<e<<'/'<<w<<" = "<<e/w<<endl;
				assert(q == e/w);
			}
			if(!q.zero()) assert(w == e/q);
		}
		cout<<"complete mult"<<endl;
	}
	// bugs
	if(BITSIZE==32){
		vector<ull> v={35,4294967294,4294967278,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,536870911,4294967291,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2};
		vector<ull> w={35,2147483647,2147483638,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,4294967295,2415919103,4294967290,2147483648,1,2147483648,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2};
		reverse(v.begin(),v.end());
		reverse(w.begin(),w.end());
		Integer a(v,1),b(w,1);
//		assert(a.abscmp(b)==1);
		F(120){
			a=b*a;
		}
	}
	// basics
	{
		Integer a=2000;
		FOR(i,1,10000){
			assert(a/i == (2000/i));
		}
		FOR(i,1,10000){
			assert(a%i == (2000%i));
		}
		cout<<"complete basics"<<endl;
	}
	{
		vector<ull> v={12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		reverse(v.begin(),v.end());
		Integer c(v,1);
		Rational r(c,1);
		cout<<sqrt(r)<<endl;
		cout<<"sqrt complete\n";
	}
	// initialization
	{
		Rational a=5,b(0);
		auto res=1/(2*a);
	}
	{
		Rational r(40,20),q(10,20);
		r=r*q;
		Integer t=(r.c%r.j);
		t=(r.c%r.j);
		cout<<"basics complete\n";
	}
	// testing previous bugs
	{
		vector<ull> v={4,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		reverse(v.begin(),v.end());
		Integer r(v,1);
	}
	{
		vector<ull> v={0,0,1};
		Integer q={v,1},w(1),e={{MAXULL,MAXULL},1};
		assert(e==q-w);
	}
	{
		vector<ull> vv={194,192};
		Integer q={vv,1},w(1),e;
		e=q+w;
	}
	{
		vector<ull> v={1,1,1};
		Integer a={v,1},b(1),c;
		c=a%b; // resulted in (--127 128)
		cout<<"complete bugs"<<endl;
	}
	// to double
	{
		Integer a(16);
		assert((ld)a == 16);
		Rational r(1,2);
		assert((ld)r == 0.5);
	}
	// power
	if(MAXULL == 255){
		Integer a(2),b(4),c(16);
		assert(pow(a,b) == c);
		vector<ull> vres={3,39,203,39,52,17,157,59,122,144,0,0,0,0,0,0,0};
		reverse(vres.begin(),vres.end());
		Integer res(vres,1);
		assert(pow(Integer(20),Integer(30))==res);

		Integer w(-4);
		Rational q(2,1),rr(1,16);
		assert(pow(q,w)==rr);
		pow((Rational)2,2916);
	}
	// modulo
	{
		Integer A=20,B=7,C=6,D=21,F=0;
		assert(A%B == C);
		assert(-A%B == -C);
		assert(A%-B == C);
		assert(D%B == F);
		assert(-D%B == -F);
		assert(D%-B == F);
		cout<<"complete modulo"<<endl;
	}
	// sqrt
	{
//		cout<<"test"<<endl;
//		cout <<"res: "<< sqrt(Rational(16,1)) <<endl;
//		cout<<"test"<<endl;
//		cout<<"------------------ another tests" <<endl;
		Rational eps(1,100);
		eps=eps*eps;
		eps=eps*eps;
		FOR(i,2,1000){
//			assert(sqrt(Rational(i*i)) - Rational(i) < eps);
//			cout<<"sqrt "<<i*i<<" ok"<<endl;
		}
		cout<<"complete sqrt"<<endl;
	}
	return 0;
}
#endif

