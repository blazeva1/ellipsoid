#include <cmath>
#include <fstream>
#include <algorithm>
#include "matrix.cpp"

using namespace std;

#define NONE 0
#define DRAW 1
#define WOLFRAM 2

//typedef double numberr;
typedef Rational numberr;

//typedef unsigned long long ll;

int printType = 0;
bool expand = true;

template <class number>
void drawPolygon(const Matrix<number> & A, const Matrix<number> & b, ostream & os = cout) {
    os << "pgp " << A.getY();
    for (int i = 0; i < A.getY(); ++i) {
        os << " " << (double) A.get(i, 0) << " " << (double) A.get(i, 1) << " " << (double) b.get(i, 0);
    }
    os << endl;
}

template <class number>
void drawEllipse(const Matrix<number> & E, const Matrix<number> & z, ostream & os = cout) {
	assert(E.getX() == 2 && E.getY() == 2);
	double aie = (double) E.get(0, 0);
	double bie = (double) E.get(0, 1);
	double cie = (double) E.get(1, 1);
    bool swapAxes = false;
    bool invertAngle = false;
    if (aie < cie) {
        swap(aie, cie);
        swapAxes = true;
        invertAngle = !invertAngle;
    }
    if (bie < 0) {
        bie *= -1;
        invertAngle = !invertAngle;
    }
    double idet = aie * cie - bie * bie;
    double ae = cie / idet;
    double be = (-bie / idet)*2;
    double ce = aie / idet;
	double fi2 = ae == ce ? be == 0 ? 0 : M_PI/2 : atan(be / (ae - ce));
	double cosFi2 = cos(fi2);
	double cosSqFi = (1 + cosFi2) / 2;
	double sinSqFi = (1 - cosFi2) / 2;
	double bCosFiSinFi = be * sqrt(cosSqFi) * sqrt(sinSqFi);
	double aSq = 1 / (ae * sinSqFi - bCosFiSinFi + ce * cosSqFi);
	double bSq = 1 / (ae * cosSqFi + bCosFiSinFi + ce * sinSqFi);
    double fi = fi2 * (invertAngle ? -1 : 1) / 2;
    if (swapAxes) swap(aSq, bSq);
    os << "el " << (double) z.get(0,0) << " " << (double) z.get(1,0) << " " << fi << " " << 2 * sqrt(bSq) << " " << 2 * sqrt(aSq) << endl;
}

template <class number>
void drawSplittingLine(const Matrix<number> & a, const Matrix<number> & z, ostream & os = cout) {
    os << "dl " << (double) a.get(0,0) << " " << (double) a.get(1,0) << " " << (double) a.get(0,0) * (double) z.get(0,0) + (double) a.get(1,0) * (double) z.get(1,0) << endl;
}

template <class number>
void drawBackground(const Matrix<number> & A, const Matrix<number> & b, ostream & os = cout) {
    os << "background ";
    drawPolygon(A, b, os);
}

void printWolframInitQuery(ostream & os = cout) {
    os << "El:={};" << endl << "z:={};" << endl << "a:={};" << endl;
}

template <class number>
void printWolframAppendEllipse(const Matrix<number> & E, const Matrix<number> & z, ostream & os = cout) {
    os << "AppendTo[El,{{" << (double) E.get(0,0) << "," << (double) E.get(0,1) << "},{" << (double) E.get(1,0) << "," << (double) E.get(1,1) << "}}];" << endl;
    os << "AppendTo[z,{" << (double) z.get(0,0) << "," << (double) z.get(1,0) << "}];" << endl;
}

template <class number>
void printWolframAppendSplittingLine(const Matrix<number> & a, ostream & os = cout) {
    os << "AppendTo[a,{" << (double) a.get(0,0) << "," << (double) a.get(1,0) << "}];" << endl;
}

template <class number>
void printWolframPlotQuery(const Matrix<number> & A, const Matrix<number> & b, ostream & os = cout) {
    os << "e[x_,y_,i_]:={{x-z[[i,1]],y-z[[i,2]]}}.MatrixPower[El[[i]],-1].{x-z[[i, 1]],y-z[[i,2]]}-1;" << endl;
    os << "am[x_,y_,i_]:=a[[i,1]]x+a[[i,2]]y-(a[[i,1]]z[[i,1]]+a[[i,2]]z[[i,2]]);" << endl;
    os << "Manipulate[ContourPlot[{";
    for (int i = 0; i < A.getY(); ++i) {
        os << (double) A.get(i,0) << "x+(" << (double) A.get(i,1) << ")y==" << (double) b.get(i,0) << ",";
    }
    os << "e[x,y,Ceiling[i/3]],If[Mod[i,3]==0,e[x,y,i/3+1],0],If[Mod[i,3]!=1,am[x,y,Ceiling[i/3]],0]},{x,-r,r},{y,-r,r},ContourStyle->{";
    for (int i = 0; i < A.getY()+2; ++i) {
        os << "Automatic,";
    }
    os << "Dashed},PlotPoints->p,Epilog->{PointSize[Medium],Point[{z[[Ceiling[i/3]]],If[Mod[i,3]==0,z[[i/3+1]],z[[Ceiling[i/3]]]]}]}],{{i,1,\"Step\"},1,3*Length[El]-2,1},{{p,20,\"Precision\"},2,100,1},{{r,45,\"Scale\"},1,100,10}]" << endl;
}

template <class number>
void printInit(const Matrix<number> & A, const Matrix<number> & b, const Matrix<number> & E, ostream & os = cout) {
    if (printType == WOLFRAM) printWolframInitQuery(os);
    else if (printType == DRAW) {
        drawBackground(A, b, os);
        os << "zoom " << (double) E.get(0, 0) << endl;
    }
}

template <class number>
void printBeginFrame(const Matrix<number> & E, const Matrix<number> & z, ostream & os = cout) {
    if (printType == WOLFRAM) printWolframAppendEllipse(E, z, os);
    else if (printType == DRAW) drawEllipse(E, z, os);
}

template <class number>
void printSplittingLine(const Matrix<number> & a, const Matrix<number> & z, ostream & os = cout) {
    if (printType == WOLFRAM) printWolframAppendSplittingLine(a, os);
    else if (printType == DRAW) {
        os << "pause" << endl;
        drawSplittingLine(a, z, os);
    }
}

template <class number>
void printEndFrame(const Matrix<number> & E, const Matrix<number> & z, ostream & os = cout) {
    if (printType == DRAW) {
        os << "pause" << endl;
        drawEllipse(E, z, os);
        os << "frame" << endl;
    }
}

template <class number>
void printFinal(const Matrix<number> & A, const Matrix<number> & b, const Matrix<number> & E, const Matrix<number> & z, ostream & os = cout) {
    if (printType == WOLFRAM) {
        printWolframPlotQuery(A, b, os);
    }
}

template <class number>
bool isNotEmpty(const Matrix<number> & A, const Matrix<number> & b, const int & sphereSize = 0) {
    assert(b.getX() == 1 && A.getY() == b.getY());
    cerr << "+++ A = " << A << endl << "+++ b = " << b << endl;

    ll L = A.getBitSize() + b.getBitSize();
    ll m = A.getY();
    ll n = A.getX();
    ll N = 128 * n * n * n * L;
    ll chi = 5*N*N;

    ll L6 = 6 * L;
    Matrix<number> bNew(b);
    if (expand) {
        number eps = 1 / (pow((number) 2, L6 * m) * 2 * m);
        bNew += Matrix<number>(1, m, eps);
    }

    Matrix<number> E (n, n);
    E.fillDiagonal(sphereSize == 0 ? pow((number) 2, n * L6) : sphereSize);
    Matrix<number> z (1, n);

    const number & nn = n * n;
    const number & c1 = (number) 1 / (n + 1);
    const number & c2 = (number) 2 / (n + 1);
    const number & c3 = nn / (nn - 1);
    printInit(A, b, E);
    for (ll i = 0; i < N; ++i) {
        cerr << "+++++++++++++++++++++++++++++++++++++++++++++ ("<<i<<"/"<<N<<") ++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
        cerr << "+++ E = " << E << endl << "+++ z = " << z << endl;
        printBeginFrame(E, z);
        const Matrix<number> & Az = A * z;
        int index = Az.ltEqWithIndex(bNew);
        if (index == -1) {
            printFinal(A, b, E, z);
            return true;
        }
        cerr << "+++ a = ";
        const Matrix<number> & a = A.getVector(index);
        cerr << a << endl;
        printSplittingLine(a, z);

        const Matrix<number> & Ea = E * a;
        const Matrix<number> & at = a.transpose();
        const Matrix<number> & atEa = at * Ea;

        auto sq=sqrt(atEa.toScalar());
        z -= Ea * (c1 / sq);
        E -= (Ea * (at * E)) * (c2 / atEa.toScalar());
        E *= c3;
        printEndFrame(E, z);
    }
    return false;
}

template<class number>
bool run(int sphereSize) {
    int n, m;
    cin >> n >> m;
    Matrix<number> A (n, m);
    Matrix<number> b (1, m);
    for (int i = 0; i < m; ++i) {
        int v;
        for (int j = 0; j < n; ++j) {
            cin >> v;
            A.set(i, j, v);
        }
        cin >> v;
        b.set(i, 0, v);
    }
    return !isNotEmpty(A, b, sphereSize);
}

int main(int argc, char** argv) {
    bool precise = true, verbose = false;
    int sphereSize = 0;
    vector<string> args(argv + 1, argv + argc);
    for (string arg : args) {
        if (arg.find("-p=") == 0) {
            if (arg.substr(3) == "w") printType = WOLFRAM;
            else if (arg.substr(3) == "lv") printType = DRAW;
            else {
                cerr << "Invalid printType: " << arg.substr(3) << endl << "For help use -h" << endl;
                return -1;
            }
        }
        else if (arg.find("-s=") == 0) sphereSize = stoi(arg.substr(3));
        else if (arg == "-d") precise = false;
        else if (arg == "-ne") expand = false;
        else if (arg == "-v") verbose = true;
        else if (arg == "-h") {
            cout << "Options:" << endl;
            cout << "\t" << "-ne" << endl << "\t\t" << "disable polyhedron expansion" << endl;
            cout << "\t" << "-d" << endl << "\t\t" << "use double instead of precise rational numbers" << endl;
            cout << "\t" << "-s=<integer>" << endl << "\t\t" << "set initial sphere size" << endl;
            cout << "\t" << "-p=<w|lv>" << endl << "\t\t" << "set output format:" << endl;
            cout << "\t\t" << "w" << "\t" << "print Wolfram Mathematica script" << endl;
            cout << "\t\t" << "lv" << "\t" << "print drawing directives for LinViz" << endl;
            cout << "\t" << "-v" << endl << "\t\t" << "print computation information to stderr" << endl;
            cout << "\t" << "-h" << endl << "\t\t" << "print help" << endl;
            return -1;
        }
        else {
            cerr << "Invalid option: " << arg << endl << "For help use -h" << endl;
            return -1;
        }
    }
    if (!verbose) cerr.setstate(std::ios_base::failbit);
    if (precise) return run<Rational>(sphereSize);
    else return run<double>(sphereSize);

}
