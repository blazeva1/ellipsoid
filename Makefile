GXX=g++ 
GARGS=-Wall -pedantic -Wextra -g -std=c++11

main: numbers.cpp ellipsoid.cpp
	$(GXX) -std=c++11 -O3 ellipsoid.cpp -o ellipsoid

clean:
	rm -f ellipsoid

maintest: numbers.cpp ellipsoid.cpp
	$(GXX) $(GARGS) -DOPER ellipsoid.cpp

